LCDHype LCD Driver SDK for C++ Release 1.3
------------------------------------------

- open lcdllc.dsw
- change source code
- compile

ready!

You can now find the compiled driver dll in the driver directory and some files more. The following files can be found in the driver directory:

*.dll     -> LCD driver
info.htm  -> HTML File includes your driver description
ports.dat -> List of ports were a lcd can be connected to
specs.dat -> LCD property defaults loaded when driver is choosen

The file specs.dat containts lines describing predefined settings for a lcd type. A line must have the following content:

descripton of setting=A,B,C,D,E,F,G,Default

Therefore the letters on the right side of the equal sign have the following meaning:

A = number of columns in text mode
B = number of rows in text mode
C = execution time for text mode (normaly used for setting the delay time after data is written to lcd)
D = maximum number of custom chars supported by lcd
E = number of horizontal pixels in graphics mode
F = number of vertical pixels in graphics mode
G = execution time for graphics mode

The Keyword Default can be used to tell the driver what setting is used by default if the used hasnt selected another setting.

Copy the directory driver to the controller directory of LCDHype to use the driver.

Questions? Visit www.lcdhype.de.vu or www.lcdhypeforum.de.vu



