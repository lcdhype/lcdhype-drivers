////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// LCDHype Noritake GU192x16-800 Driver DLL Source Code                       //
//                                                                            //
// Version 1.0                                                                //
//                                                                            //
// Requirements: LCDHype Version 0.37 or above                                //
//               DriverLINX DLPortIO Driver installed                         //
//               (if not needed remove the imports)                           //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "noritake_gu192x16-800.h"
#include "windows.h"
#include "dlportIO.h" //include DriverLINX DLPortIO
#include "ks0108font.h"
#include "10x14.h"
#include "stdio.h"
            //LCD Driver DLL Identifier String
const char* ID            = "Noritake GU192x16-800 V1.0";

            //maximum Pixels of graphics mode, 192x16=3072
const int   MAX_RESPIXELS = 3072;
      
//char teststr[80]={0};
int buffersize;
bool firstcall=true;
int yplsilon;


//Custom Char Data Structure
struct TCharData 
{ BYTE position;   //Position of custom char in ram
  BYTE data[8][8]; //custom char 8x8 pixel data matrix
};

//LCD Driver Information Structure
struct TDLLInfo
{
  char IDArray[256];          //holds identifier of DLL
  bool SupportGfxLCD;         //does this driver support graphical LCDs?
  bool SupportTxtLCD;         //does this driver support text
  bool SupportLightSlider;    //does this driver support the light control slider
  bool SupportContrastSlider; //does this driver support the contrast slider
  bool SupportOutports;		  //does this driver support outports for controlling external circuits
  BYTE CCharWidth;            //custom char width in pixels
  BYTE CCharHeight;           //custom char height in pixels
  BYTE FontPitch;             //fontpitch of LCD in pixels
};

typedef unsigned short      WORD;

const char* LCDHSHAREDMEMORYID = "LCDHypeSharedConfigData";

struct TSharedConfigData
{ 
	bool isTextLCD;        //true when LCD supports text mode
	WORD LCDx;             //number of columns in text mode
	WORD LCDy;             //number of rows in text mode
	bool isGraphicLCD;     //true when LCD supports graphic mode
	WORD LCDgx;            //number of pixels in horizontal direction
	WORD LCDgy;            //number of pixels in vertical direction
	bool VerticalOutput;   //true when vertical output is enabled
	char Controller[256];  //relative path of the controller dll
	int  CurrentScript;    //index of the current script
	int  Status;           //is 0 when paused, stopped or 1 when running
};

typedef TSharedConfigData* pSharedConfigData;


//Structure for Symbol LCDs
struct TSymbolInfo
{ 
  bool SymbolData[20]; //is symbol x displayed or not?
};

//global storage for LCD propertys
bool  LCD_LightOn     = true;  //is lcd light on?
ULONG LCD_Port        = 0x378; //port where lcd is connected to
DWORD LCD_Exectime    = 10;    //execution time of lcd in textmode
DWORD LCD_ExectimeGfx = 10;    //execution time of lcd in graphics mode
DWORD LCD_Width       = 192;   //width of LCDs graphics mode in pixels
DWORD LCD_Height      = 16;   //height of LCDs graphics mode in pixels 
DWORD LCD_Cols        = 24;    //number of chars per line in text mode
DWORD LCD_Rows        = 2;    //number of LCD lines in text mode
LONG  LCD_FontPitch   = 6;    //fontpitch of LCD in pixels   
char  LCD_contrast    = 0x4f;

/****prototype defs****/
void Delay(long microseconds);
void WriteCommand(unsigned char cmd);
void WriteData(unsigned char dat);
void SetXAddress(unsigned char x);
void SetYAddress(unsigned char y);
void ClearDisplay(void);
void WriteText(char* string,int row);
void InitLCD(void);
/**********************/


//Wait function
void Delay(long microseconds)
{
	int i;
	for (i= 0;i<microseconds;i++)
	{
		DlPortReadPortUchar(0x220);
	}

}

//function used to send data to lpt port based on lcd controller

void WriteCommand(unsigned char cmd)
{
	DlPortWritePortUchar(LCD_Port+2, 0x0c);
	Delay(LCD_ExectimeGfx);
	DlPortWritePortUchar(LCD_Port+2, 0x05);
	DlPortWritePortUchar(LCD_Port, cmd);
	Delay(LCD_ExectimeGfx);
	DlPortWritePortUchar(LCD_Port+2, 0x01);
	Delay(LCD_ExectimeGfx);
	DlPortWritePortUchar(LCD_Port+2, 0x05);
	Delay(LCD_ExectimeGfx);
	DlPortWritePortUchar(LCD_Port+2, 0x0c);
}

void WriteData(unsigned char dat)
{
	DlPortWritePortUchar(LCD_Port+2, 0x04);
	Delay(LCD_ExectimeGfx);
	DlPortWritePortUchar(LCD_Port+2, 0x0d);
	DlPortWritePortUchar(LCD_Port, dat);
	Delay(LCD_ExectimeGfx);
	DlPortWritePortUchar(LCD_Port+2, 0x09);
	Delay(LCD_ExectimeGfx);
	DlPortWritePortUchar(LCD_Port+2, 0x0d);
	Delay(LCD_ExectimeGfx);
	DlPortWritePortUchar(LCD_Port+2, 0x04);
}

void SetXAddress(unsigned char x)
{
	WriteCommand(0x64);
	WriteCommand(x);
}

void SetYAddress(unsigned char y)
{
	WriteCommand(0x60);
	WriteCommand(y);	
}

void ClearDisplay(void)
{
	WriteCommand(0x5f);
}



void WriteText(char* string,int row)
	{
	unsigned int n, i;

	SetXAddress(0);
	SetYAddress(row);

	for (n = 0;n < strlen(string);n++)
	{
	  for(i=0;i<5;i++)
	  {
		WriteData(font5x7[((string[n]-32))*5+i]);  // overlay graphics and text
	  }
	  WriteData(0);
	}
}



void InitLCD(void)
{
	WriteCommand(0x5f);
	Delay(1000);
	WriteCommand(0x62);	//start assign GRAM
	WriteCommand(0x00);
	WriteData(0xff);
	WriteCommand(0x62);
	WriteCommand(0x01);
	WriteData(0xff);
	WriteCommand(0x62);
	WriteCommand(0x02);
	WriteData(0xff);
	WriteCommand(0x62);
	WriteCommand(0x03);
	WriteData(0xff);
	WriteCommand(0x62);
	WriteCommand(0x04);
	WriteData(0xff);
	WriteCommand(0x62);
	WriteCommand(0x05);
	WriteData(0xff);	//end assign GRAM
	WriteCommand(0x64); //GRAM write-position set x
	WriteCommand(0x00);
	WriteCommand(0xb4); //GRAM write-position set y
	WriteCommand(0x24);	//only layer 0
	WriteCommand(0x40);	//0, GRAM on, 0, non-inverse
	WriteCommand(0x5f);
	WriteCommand(0x70); //GRAM start-position setup x
	WriteCommand(0x00);
	WriteCommand(0x84); //auto-inc x	
	WriteCommand(0x4f);

}


//function used to create a lcd driver config dialog
//dont export this function if you dont want to create config dialog
//void _stdcall LCD_ConfigDialog()
//{ };

//function used to send a byte to lcd controller
void _stdcall LCD_SendToController(BYTE Value)
{ 
	WriteCommand(Value); 
};


void _stdcall LCD_SendToMemory(BYTE Value)
{ 
	unsigned int  i;

	if (yplsilon == 1) 
	{
		unsigned char buffer1, buffer2, h1,h2,h3,h4,h5,h6,h7,h8,i1,i2,i3,i4,i5,i6,i7,i8;

		if ((Value<32)||(Value>(96+32))) return;
		for(i=0;i<10;i++)
		{
			WriteCommand(0x80); //Zeilenvorschub aus
			
			buffer1 = font10x14[((Value-32))*20+(i*2)];
				h1 = 0x01 & buffer1;
				h1 = h1 << 7;
				h2 = 0x02 & buffer1;
				h2 = h2 << 5;
				h3 = 0x04 & buffer1;
				h3 = h3 << 3;
				h4 = 0x08 & buffer1;
				h4 = h4 << 1;
				h5 = 0x10 & buffer1;
				h5 = h5 >> 1;
				h6 = 0x20 & buffer1;
				h6 = h6 >> 3;
				h7 = 0x40 & buffer1;
				h7 = h7 >> 5;
				h8 = 0x80 & buffer1;
				h8 = h8 >> 7;
				buffer1 = h1 | h2 | h3 | h4 | h5 | h6 | h7 | h8;
				buffer1 = buffer1 << 1;

			buffer2 = font10x14[((Value-32))*20+((i*2)+1)];
				i1 = 0x01 & buffer2;
				i1 = i1 << 7;
				i2 = 0x02 & buffer2;
				i2 = i2 << 5;
				i3 = 0x04 & buffer2;
				i3 = i3 << 3;
				i4 = 0x08 & buffer2;
				i4 = i4 << 1;
				i5 = 0x10 & buffer2;
				i5 = i5 >> 1;
				i6 = 0x20 & buffer2;
				i6 = i6 >> 3;
				i7 = 0x40 & buffer2;
				i7 = i7 >> 5;
				i8 = 0x80 & buffer2;
				i8 = i8 >> 7;
				buffer2 = i1 | i2 | i3 | i4 | i5 | i6 | i7 | i8;
				buffer2 = buffer2 << 1;
				h1 = h1 >> 7;
				buffer2 = buffer2 | h1;

			WriteData(buffer1);		//ausgabe
			SetYAddress(1);			//untere 8 bit schreiben
			WriteCommand(0x84);		//Zeilenvorschub ein
			WriteData(buffer2);		//ausgabe
			SetYAddress(0);	
		}
		WriteData(0);
	}
	else {
		if ((Value<32)||(Value>(96+32))) return;
			for(i=0;i<5;i++)
			{
				WriteData(font5x7[((Value-32))*5+i]);  // overlay graphics and text
			}
			WriteData(0);
		 };
 };



//function used to send gfx data to graphical lcd
void _stdcall LCD_SendToGfxMemory(BYTE Pixels[MAX_RESPIXELS], //pixel color data
                                  WORD x1,                    //refresh area x1,y1,x2,y2
                                  WORD y1, 
                                  WORD x2,
                                  WORD y2,
                                  bool inverted               //output of gfx data inverted?
                                 )
{ 
	  unsigned char i, col, page, lcdbyte;

	  int ystartlcd, yendlcd, tempVal;
	
	  if ((x2>LCD_Width)||(y2>LCD_Height)) return;
	  //sprintf(teststr,"x1 %u y1 %u x2 %u y2 %u",x1,y1,x2,y2);
	  //MessageBox(NULL, teststr , "Title Bar", MB_OK);

	  ystartlcd = y1>>3;
	  yendlcd = y2>>3;


	  for(page=ystartlcd;(page<=yendlcd)&&(page<LCD_Rows);page++)
	  {
		SetYAddress(page);
		SetXAddress((char)x1);
		tempVal = (page*LCD_Width)<<3;	//for speed reasons ;)

		for(col=(char)x1;(col<=x2)&&(col<LCD_Width);col++)
		{
			lcdbyte = 0;
			for(i=7;i>0;i--)
			{
			  lcdbyte |= (Pixels)[tempVal + i*LCD_Width + col] & 1;
			  lcdbyte <<= 1;
			}
			lcdbyte |= (Pixels)[tempVal + col] & 1;
			if (inverted) lcdbyte^=0xff;
			WriteData(lcdbyte);
		}
	  }
};

//function to set the output address in text mode
void _stdcall LCD_SetOutputAddress(DWORD x, DWORD y)
{
	if ((x>LCD_Cols)||(y>LCD_Rows)) return;
	SetXAddress((char)x);
	SetYAddress((char)y);
};

//function to write a custom char to ram
void _stdcall LCD_SetCGRAMChar(TCharData cdata)
{ 

};

//function to get the position of custom char
//IN : custom char position based on HD44780 LCDs (0...)
//OUT: custom char position converted to current lcd controllers position
//
//Example : HD44780, first custom char is at position 0
//          T6963c , first custom char is at position 128
//
//          function LCD_GetCGRAMChar gets 1 and returns 128
BYTE _stdcall LCD_GetCGRAMChar(BYTE position)
{ return position; }; 

//function used to initialize LCD
void _stdcall LCD_Init(void)
{ 
	InitLCD();
	buffersize=LCD_Width * ((LCD_Height%8==0) ? LCD_Height/8 : LCD_Height/8+1);

	if(firstcall)
	{
		firstcall=false;
		SetXAddress(0);
		SetYAddress(0);
	}
	SetYAddress(0);
	SetXAddress(0);
};

//function used to clean up driver usage, called when DLL is closed.
void _stdcall LCD_CleanUp()
{ 	
//MessageBox(NULL, "Clear" , "Title Bar", MB_OK);
	ClearDisplay();
};

//function used to indicate that the lcd can receive data
//commonly used when interfacing serial lcds else return value true
bool _stdcall LCD_IsReadyToReceive()
{ return true; }; 

//function used to set the lcd propertys, so that the lcd driver can work correctly
//commonly used when supporting more than one lcd type in one driver
void _stdcall LCD_SetIOPropertys(char* Port,                //port lcd is connected to
								 DWORD Exectime,            //execution time for text mode
								 DWORD ExectimeGfx,         //execution time for gfx mode
								 DWORD X,                   //cols in text mode
								 DWORD Y,                   //rows in text mode
								 DWORD GX,                  //width in pixels in gfx mode
								 DWORD GY,                  //height in pixels in gfx mode
								 bool  LightOn,             //is light control enabled?
								 BYTE  LightSliderValue,    //brightness value from light slider
								 bool  ContrastOn,          //is contrast control enabled?
								 BYTE  ContrastSliderValue, //contrast value from contrast slider
								 DWORD Outports,            //32bit = 32 outports, bit=0=off, bit=1=on 
								 bool  UnderlineMode,       //is underline mode in text mode enabled?
								 bool  UnderlineOutput      //display underlined text?
								)
{ 
	sscanf(Port, "%03X", &LCD_Port);
	//Exectime;            //execution time for text mode
	LCD_ExectimeGfx=ExectimeGfx;         //execution time for gfx mode
	X;                   //cols in text mode
	Y;                   //rows in text mode
	GX;                  //width in pixels in gfx mode
	GY;                  //height in pixels in gfx mode
	LightOn;             //is light control enabled?
	LightSliderValue;    //brightness value from light slider
	Outports;            //32bit = 32 outports, bit=0=off, bit=1=on 
	//UnderlineMode;       //is underline mode in text mode enabled?
	//UnderlineOutput;  

	yplsilon = Y;
//WriteCommand(0x44);
	LightSliderValue = ~LightSliderValue;
	WriteCommand(0x40 | LightSliderValue>>4);

	if((ContrastOn) && (LCD_contrast!=ContrastSliderValue))
	{
		LCD_contrast=ContrastSliderValue; //contrast value from contrast slider
		//WriteCommand(0x40 +  LCD_contrast>>4); // Electronic Volume Mode Set	
	}
	else
	{
		Delay(LCD_ExectimeGfx);
		//WriteCommand(0x40); // Electronic Volume Mode Set	
	}

};

//function used to interface symbols of a symbol lcd
void _stdcall LCD_Customize(TSymbolInfo SymData)
{ };

//function used to return information about the dll to the calling process
TDLLInfo _stdcall DLL_GetInfo()
{ 
  TDLLInfo result;  

  result.CCharWidth  = 6;       
  result.CCharHeight = 8;
  result.FontPitch = 6;
  result.SupportGfxLCD = true;
  result.SupportTxtLCD = true;
  result.SupportLightSlider = true;
  result.SupportContrastSlider = false;
  result.SupportOutports = false;

  ZeroMemory(&result.IDArray,256);
  CopyMemory(&result.IDArray[0],&ID[0],strlen(ID));

  return result;
};

