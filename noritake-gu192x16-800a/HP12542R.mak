# Microsoft Developer Studio Generated NMAKE File, Based on lcdllc.dsp
!IF "$(CFG)" == ""
CFG=lcdllc - Win32 Debug
!MESSAGE No configuration specified. Defaulting to lcdllc - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "lcdllc - Win32 Release" && "$(CFG)" != "lcdllc - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "lcdllc.mak" CFG="lcdllc - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "lcdllc - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "lcdllc - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "lcdllc - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release

ALL : ".\driver\lcd_driver.dll"


CLEAN :
	-@erase "$(INTDIR)\lcdllc.obj"
	-@erase "$(INTDIR)\lcdllc.pch"
	-@erase "$(INTDIR)\lcdllc.res"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\lcd_driver.exp"
	-@erase ".\driver\lcd_driver.dll"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /Fp"$(INTDIR)\lcdllc.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x407 /fo"$(INTDIR)\lcdllc.res" /d "NDEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\lcdllc.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=dlportio.lib /nologo /subsystem:windows /dll /pdb:none /machine:I386 /def:".\lcdllc.def" /force /out:"driver/lcd_driver.dll" /implib:"$(OUTDIR)\lcd_driver.lib" 
DEF_FILE= \
	".\lcdllc.def"
LINK32_OBJS= \
	"$(INTDIR)\lcdllc.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\lcdllc.res"

".\driver\lcd_driver.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "lcdllc - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug

ALL : ".\driver\lcd_driver.dll"


CLEAN :
	-@erase "$(INTDIR)\lcdllc.obj"
	-@erase "$(INTDIR)\lcdllc.pch"
	-@erase "$(INTDIR)\lcdllc.res"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\lcd_driver.exp"
	-@erase ".\driver\lcd_driver.dll"
	-@erase ".\driver\lcd_driver.ilk"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /Gz /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /Fp"$(INTDIR)\lcdllc.pch" /Yu"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x407 /fo"$(INTDIR)\lcdllc.res" /d "_DEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\lcdllc.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=dlportio.lib /nologo /subsystem:windows /dll /incremental:yes /pdb:"$(OUTDIR)\lcd_driver.pdb" /machine:I386 /def:".\lcdllc.def" /out:"driver/lcd_driver.dll" /implib:"$(OUTDIR)\lcd_driver.lib" /pdbtype:sept 
DEF_FILE= \
	".\lcdllc.def"
LINK32_OBJS= \
	"$(INTDIR)\lcdllc.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\lcdllc.res"

".\driver\lcd_driver.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("lcdllc.dep")
!INCLUDE "lcdllc.dep"
!ELSE 
!MESSAGE Warning: cannot find "lcdllc.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "lcdllc - Win32 Release" || "$(CFG)" == "lcdllc - Win32 Debug"
SOURCE=.\lcdllc.cpp

"$(INTDIR)\lcdllc.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\lcdllc.pch"


SOURCE=.\lcdllc.rc

"$(INTDIR)\lcdllc.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)


SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "lcdllc - Win32 Release"

CPP_SWITCHES=/nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /Fp"$(INTDIR)\lcdllc.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\lcdllc.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "lcdllc - Win32 Debug"

CPP_SWITCHES=/nologo /Gz /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /Fp"$(INTDIR)\lcdllc.pch" /Yc"stdafx.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

"$(INTDIR)\StdAfx.obj"	"$(INTDIR)\lcdllc.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ENDIF 


!ENDIF 

